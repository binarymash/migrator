﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MvcApplication1.Controllers
{
    using Models;

    public class MigrationController : Controller
    {
        //
        // GET: /Transfer/Create

        public ActionResult Create()
        {
            var model = new Migration
            {
                Context = new ContextDefinition(),
                Order = new Order()
            };
            return View(model);
        } 

    }
}
