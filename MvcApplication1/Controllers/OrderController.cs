﻿using System.Web.Mvc;

namespace MvcApplication1.Controllers
{
    using Models;

    public class OrderController : Controller
    {

        //
        // POST: /OrderSpecification/Create

        [HttpPost]
        public ActionResult Create(Order model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new JsonErrorResult("blah", null, ModelState));
            }

            try
            {
                return Json(new JsonGoodResult("ok", null));
            }
            catch
            {
                return Json(
                    new
                    {
                        Success = false,
                        Message = "Something bad happened. Try again."
                    });
            }
        }
        

    }
}
