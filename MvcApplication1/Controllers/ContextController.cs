﻿using System.Web.Mvc;

namespace MvcApplication1.Controllers
{
    using System.Collections.Generic;
    using Models;

    public class ContextController : Controller
    {

        public ActionResult Load(ContextDefinition model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new JsonErrorResult("ModelState", null, ModelState));
            }

            try
            {                
                var context = GetContext(model.AccountId, model.Username, model.Password);

                //store some data on session, eg for products?

                return Json(new JsonGoodResult("ok", context));
            }
            catch
            {
                return Json(
                    new
                    {
                        Success = false,
                        Message = "Something bad happened. Try again."
                    });
            }
        }

        private static Context GetContext(int accountId, string username, string password)
        {
            ContextDefinition model;
            Context context;
            if (accountId == 123)
            {
                context = new Context
                {
                    AccountId = accountId,
                    AccountDescription = "My Account",
                    AvailableContacts = new List<Contact>
                    {
                        new Contact {Id = 123, FirstName = "Dave", LastName = "Smith"},
                        new Contact {Id = 456, FirstName = "Joe", LastName = "Blogs"}
                    },
                    Username = username
                };
            }
            else
            {
                context = new Context
                {
                    AccountId = accountId,
                    AccountDescription = "My Other Account",
                    AvailableContacts = new List<Contact>
                    {
                        new Contact {Id = 123, FirstName = "Aaron", LastName = "Aardvark"},
                        new Contact {Id = 456, FirstName = "Zebedee", LastName = "Zyzxx"}
                    },
                    Username = username
                };
            }
            return context;
        }
    }
}
