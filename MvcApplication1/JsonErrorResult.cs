﻿
namespace MvcApplication1
{
    using System.Collections.Generic;
    using System.Web.Mvc;

    public class JsonErrorResult : JsonResult
    {
        public List<Models.Error> Errors { get; set; }
 

        public JsonErrorResult(string message, object data, ModelStateDictionary modelState) : base(false, message, data)
        {
            Errors = new List<Models.Error>();
            foreach (var key in modelState.Keys)
            {
                foreach (var error in modelState[key].Errors)
                { 
                    Errors.Add(new Models.Error
                        {
                            Id = key, 
                            Message = error.ErrorMessage
                        });
                }

            }
        }
    }

}