﻿using System.Collections.Generic;

namespace MvcApplication1.Models
{
    public class Context
    {
        public int AccountId { get; set; }
        public string AccountDescription { get; set; }
        public List<Contact> AvailableContacts { get; set; }
        public string Username { get; set; }
    }
}