﻿namespace MvcApplication1.Models
{
    using System.ComponentModel.DataAnnotations;

    public class ContextDefinition
    {
        [Required(ErrorMessage = "You must enter an account ID")]
        public int AccountId { get; set; }

        [Required(ErrorMessage = "You must enter a username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "You must enter a password")]
        public string Password { get; set; }
    }
}