﻿namespace MvcApplication1.Models
{
    using System.ComponentModel.DataAnnotations;

    public class OrderItem
    {
        [Required(ErrorMessage = "You must enter an account ID")]
        public string DomainName { get; set; }

        [Required(ErrorMessage = "You must enter a Price")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "You must enter a thing")]
        public string Thing { get; set; }
    }
}