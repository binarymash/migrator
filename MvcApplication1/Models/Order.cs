﻿namespace MvcApplication1.Models
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class Order
    {
        [Required(ErrorMessage = "You must specify an account ID")]
        public int AccountId { get; set; }

        [Required(ErrorMessage = "You must specify a username")]
        public string Username { get; set; }

        [Required(ErrorMessage = "You must specify some value")]
        public string SomeValue { get; set; }

        [Required(ErrorMessage = "You must specify some items")]
        public List<OrderItem> OrderItems { get; set; }

        [Required(ErrorMessage = "You must secify a contact")]
        public int ContactId { get; set; }
    }
}