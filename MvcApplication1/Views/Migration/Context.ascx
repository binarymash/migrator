﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MvcApplication1.Models.ContextDefinition>" %>

    <% using (Html.BeginForm("Load", "Context", FormMethod.Post, new {@id = "contextForm"})) {%>
        <fieldset>          
        <div class="editor-label">
            <%: Html.LabelFor(model => model.AccountId) %>
        </div>
        <div class="editor-field">
            <%: Html.TextBoxFor(model => model.AccountId, new Dictionary<string, object> {{ "data-bind", "value:accountId" }})%>
        </div>
            
        <div class="editor-label">
            <%: Html.LabelFor(model => model.Username) %>
        </div>
        <div class="editor-field">
            <%: Html.TextBoxFor(model => model.Username, new Dictionary<string, object> {{ "data-bind", "value:username" }})%>
        </div>
            
        <div class="editor-label">
            <%: Html.LabelFor(model => model.Password) %>
        </div>
        <div class="editor-field">
            <%: Html.TextBoxFor(model => model.Password, new Dictionary<string, object> {{ "data-bind", "value:password" }})%>
        </div>
            
        <p>
            <input type="submit" value="Load Account and Accreditation" />
        </p>
        </fieldset>
    <% } %>

