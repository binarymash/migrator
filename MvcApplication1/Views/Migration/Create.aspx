﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<MvcApplication1.Models.Migration>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Index</title>
    <script src="../../Scripts/jquery-1.4.1.js" type="text/javascript"></script>
    <script src="../../Scripts/knockout-2.1.0.js" type="text/javascript"></script>
    <script src="http://malsup.github.com/jquery.form.js" type="text/javascript"></script> 
</head>
<body>
    
<script type="text/javascript">

    var ErrorsViewModel = function () {
        this.errors = ko.observableArray([]);
    };

    var ContextViewModel = function () {
        this.accountId = ko.observable('');
        this.username = ko.observable('');
        this.password = ko.observable('');
    };

    var OrderViewModel = function () {
        this.accountId = ko.observable('');
        this.username = ko.observable('');
        this.password = ko.observable('');
        this.someValue = ko.observable('');
        this.accountDescription = ko.observable('');
        this.availableContacts = ko.observableArray([]);
        this.selectedContact = ko.observable('');
        this.orders = ko.observableArray([]);
        this.orderIsEditable = ko.observable(false);
        
        this.SetUpOrderContext = function(data) {
            this.accountId(data.AccountId);
            this.accountDescription(data.AccountDescription);
            this.availableContacts(data.AvailableContacts);
            this.username(data.Username);
            this.orderIsEditable(true);
        };
    };

    var AjaxStatusViewModel = function () {
        this.contextLoadFinished = ko.observable(false);
        this.submitOrderFinished = ko.observable(false);
        this.isVisible = ko.observable(false);
    };

    var errorsViewModel = new ErrorsViewModel();
    var contextViewModel = new ContextViewModel();
    var orderViewModel = new OrderViewModel();
    var ajaxStatusViewModel = new AjaxStatusViewModel();
    
    $(function () {

        ko.applyBindings(errorsViewModel, $("#errors")[0]);
        ko.applyBindings(contextViewModel, $("#contextForm")[0]);
        ko.applyBindings(orderViewModel, $("#orderForm")[0]);
        ko.applyBindings(ajaxStatusViewModel, $("#ajaxStatus")[0]);        

        $("#contextForm").ajaxForm({
            success: loadContextSuccess,
            error: loadContextError
        });

        $("#orderForm").ajaxForm({
            success: submitOrderSuccess,
            error: submitOrderError
        });
               
    });   

    function loadContextSuccess(ctx) {
        errorsViewModel.errors(ctx.Errors);
        if (ctx.Success) {
            orderViewModel.SetUpOrderContext(ctx.Data);
        }
        ajaxStatusViewModel.contextLoadFinished(true);
    }

    function loadContextError() {
        alert("An error occurred when attempting to load the account - try again.");
        ajaxStatusViewModel.contextLoadFinished(true);
    };

        
    function submitOrderSuccess(ctx) {
        errorsViewModel.errors(ctx.Errors);
        if (ctx.Success) {
            alert('order submitted');
        }
        ajaxStatusViewModel.submitOrderFinished(true);
    };

    function submitOrderError() {
        alert("An error occurred when submitting the account - try again.");
        ajaxStatusViewModel.submitOrderFinished(true);
    };
    
</script>

    <div id="errors" data-bind="foreach:errors">
       <p data-bind="text:Message"></p>
    </div>
    
    <div>
        <% Html.RenderPartial("Context", Model.Context); %>
    </div>

    <div>
        <% Html.RenderPartial("Order", Model.Order); %>
    </div>

    <div id="ajaxStatus" data-bind="visible:isVisible">
      <p>Context Load Finished: <span id="contextLoadFinished" data-bind="text:contextLoadFinished"></span></p>
      <p>Submit Order Finished: <span id="submitOrderFinished" data-bind="text:submitOrderFinished"></span></p>
    </div>

</body>
</html>
