﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<MvcApplication1.Models.Order>" %>
<%@ Import Namespace="System.ComponentModel" %>

    <% using (Html.BeginForm("Create", "Order", FormMethod.Post, new {@id = "orderForm"})) {%>
    <fieldset data-bind="enable:orderIsEditable">
        <div class="editor-label">                 
            GSAP Account: <span data-bind="text:accountDescription"></span>
            <%: Html.HiddenFor(model => model.AccountId, new Dictionary<string, object>{{"data-bind", "value:accountId"}}) %>
        </div>

        <div class="editor-label">
            GRP Username: <span data-bind="text:username"></span>
            <%: Html.HiddenFor(model => model.Username, new Dictionary<string, object>{{"data-bind", "value:username"}}) %>
        </div>    
            
        <div class="editor-label">
            <%: Html.LabelFor(model => model.SomeValue) %>
        </div>
        <div>
            <%: Html.TextBoxFor(model => model.SomeValue)%>
        </div>    

        <div class="editor-label">
            <%: Html.LabelFor(model => model.ContactId) %>
        </div>
        <div class="editor-field">
        <%: Html.DropDownListFor(model => model.ContactId, new BindingList<SelectListItem>(), new Dictionary<string, object>{{"data-bind", 
                "options:availableContacts, " +
                "optionsText:function(item){return item.FirstName + ' ' + item.LastName + ' [' + item.Id + ']';}, " +
                "value:selectedContact, " +
                "optionsCaption:'Choose...', " +
                "optionsValue:'Id'"}}) %>            
        </div>
        
        <table>
        <thead>
            <tr>
                <th>Domain Name</th>
                <th>Price</th>
                <th>Thing</th>
            </tr>
        </thead>
        <tbody data-bind="foreach:orders">
        <tr ><td><input type="text" data-bind="DomainName"/></td></tr>
        <tr ><td><input type="text" data-bind="Price"/></td></tr>
        <tr ><td><input type="text" data-bind="Thing"/></td></tr>
        </tbody>
        </table>
        <p>
            <input type="submit" value="Submit order" />
        </p>
        </fieldset>
    <% } %>


