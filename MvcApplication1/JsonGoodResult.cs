﻿
namespace MvcApplication1
{
    public class JsonGoodResult : JsonResult
    {
        public JsonGoodResult(string message, object data) : base(true, message, data)
        {
        }
    }
}